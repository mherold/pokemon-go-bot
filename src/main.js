import PokeAPI from "pokemongo-api";

/**
 * Set environment variables or replace placeholder text
 */
var locationEsslingen = {
    latitude: 48.740492,
    longitude: 9.308461
};

var location = {
    latitude: 48.62662,
    longitude: 9.3287213
};

const releasePokemonsLowerThan = 2;

/**
 * Release or evolve pokemon
 *
 * @param Poke
 */
var cleanupPokemons = async function (Poke) {

    try {
        let inventory = await Poke.GetInventory();

        for (let pokemon of inventory.pokemons) {

            // release pokemon with cp < 200
            if (pokemon.cp < releasePokemonsLowerThan) {
                console.log('releasing', pokemon.name);
                console.log(await pokemon.release());
                await sleep(500);
            } else {
                // try to evolve pokemon if enough candy is available
                for (let candy of inventory.candies) {
                    if (pokemon.pokemon_id == candy.family_id
                        || (pokemon.prev_evolution && pokemon.prev_evolution[0].id == candy.family_id)) {
                        if (pokemon.candy.includes('500') && candy.candy >= 500) {
                            console.log('EVOLVING: ', pokemon.name);
                            await pokemon.evolve();
                        } else if (pokemon.candy.includes('50') && candy.candy >= 50) {
                            console.log('EVOLVING: ', pokemon.name);
                            await pokemon.evolve();
                        } else if (pokemon.candy.includes('25') && candy.candy >= 25) {
                            console.log('EVOLVING: ', pokemon.name);
                            await pokemon.evolve();
                        }
                        else if (pokemon.candy.includes('12') && candy.candy >= 12) {
                            console.log('EVOLVING: ', pokemon.name);
                            await pokemon.evolve();
                        }
                    }
                }
            }
        }
    } catch (error) {
        console.log(error);
    }
};

/**
 * Sleeps an amount of time
 *
 * @param ms
 * @returns {Promise}
 */
function sleep(ms = 0) {
    return new Promise(r => setTimeout(r, ms));
}

/**
 * Starts a new bot
 *
 * @param username
 * @param password
 * @returns {*}
 */
async function startBot(username, password) {

    // basic setup
    let nextPosition = null;
    let lastPosition;

    // login at pokemon trainers club (ptc)
    const pokeApi = new PokeAPI();
    pokeApi.player.location = location;
    await pokeApi.login(username, password, 'ptc');

    /**
     * Catches pokemons in range
     *
     * @param objects
     */
    var tryCatchingPokemons = async function (objects) {
        for (let pokemon of objects.catchable_pokemons) {
            console.log('Trying to catch', pokemon.name);
            try {
                await pokemon.encounterAndCatch();
                await sleep(1000);
            } catch (error) {
                console.log(error);
            }
        }
    };

    /**
     * Returns nearby pokestop, which isn't on cooldown
     *
     * @param objects
     * @returns {*}
     */
    var getNextPokeStop = function (objects) {
        for (let pokestop of objects.forts.checkpoints) {
            // pokestop does things close to you
            if (!pokestop.cooldown) {
                return pokestop;
            }
        }
    };

    /**
     * Refreshes player
     * cleans up pokemons
     * catches new ones of there are some
     * walks to nearby pokestops
     *
     * @returns {boolean}
     */
    var loop = async function () {
        console.log('refresh Player');

        let objects;

        // getting player data
        try {
            await pokeApi.GetPlayer();
            console.log('download Map');
            objects = await pokeApi.GetMapObjects();

            // throw away powerless pokemons and try to evolve the others
            await cleanupPokemons(pokeApi);
        } catch (error) {
            console.log(error);
            await sleep(4000);
            return false;
        }

        // catch pokemons
        try {
            await tryCatchingPokemons(objects);
        } catch (error) {
            //TODO: handle exception
        }

        // get position of next pokestop
        if (nextPosition === null) {
            nextPosition = getNextPokeStop(objects);
        }

        // go to pokestop if one is nearby
        if (nextPosition) {
            let result = await pokeApi.player.walkToPoint(nextPosition.latitude, nextPosition.longitude);

            // loot pokestop
            if (result === true) {
                nextPosition.search();
                lastPosition = nextPosition;
                nextPosition = null;
            }
        }

        console.log('walking around');

        // run in a random direction if no pokestop is around
        if (nextPosition === null) {
            await pokeApi.player.walkAround();
        }

    };

    while (true) {
        let loopResult = await loop();
        await sleep(3700);

        if (loopResult === false) {
            break;
        }
    }

    return await startBot(username, password);
}

startBot('mash1t', 'Kokosnuss0_1').catch(console.log);
// startBot('casanobi', 'casanobi').catch(console.log);
// startBot('mastertest1', 'mastertest1').catch(console.log);
