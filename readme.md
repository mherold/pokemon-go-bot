Pokémon Go Bot
==============

Version
-------

0.1.0

Installation
------------

Folgende Befehle müssen zur Installation im Root-Verzeichnis ausgeführt werden:

```
$ npm install
```

Zusatz
------

Aktuell wird ein eigener Fork der Pokémon Go Api eingesetzt, welcher in der Datei ``pokemongo-api.zip`` zu finden ist.

Dieses Archiv muss in den Ordner ``node_modules`` entpackt und anstelle der dort verfügbaren ``pokemongo-api`` eingefügt werden.

Dieser Schritt wird, sobald der Pull-Request akzeptiert wurde, entfernt.

Start des Bots
--------------

Der Bot kann über folgenden Befehl gestartet werden:

```
$ npm run main
```

Weitere Direktiven und Scripts können in der Datei ``package.json`` im Bereich ``scripts`` zur Verfügung gestellt werden.

Architektur
-----------

Dieser Bot ist in Ecma-Script 6 geschrieben.

Durch den Einsatz von Babel ist dieses direkt ausführbar.

Features
--------

* Laufen
* Fangen
* Schwache Pokémon (unter 200 CP) freilassen
* Entwickeln, falls genügend Bonbons im Inventar sind

ToDo's
------

* PokéStops farmen